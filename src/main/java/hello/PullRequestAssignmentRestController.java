package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static java.lang.String.format;

@RestController
public class PullRequestAssignmentRestController {

    @Autowired
    private SimpMessagingTemplate template;

    @RequestMapping("/assign")
    public void greeting(@RequestParam(value="team") String team, @RequestParam(value="pr") String pullRequest) {
        String assignment = format("Pull request %s has been assigned to %s", pullRequest, team);
        template.convertAndSend("/topic/assignments", new PullRequestAssignment(assignment));
    }
}
