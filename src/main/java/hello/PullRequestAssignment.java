package hello;

public class PullRequestAssignment {

    private String assignment;

    public PullRequestAssignment() {
    }

    public PullRequestAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getAssignment() {
        return assignment;
    }
}
